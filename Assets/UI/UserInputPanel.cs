﻿using System;
using System.Collections;
using System.Collections.Generic;
using CW;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.UI;

public class UserInputPanel : MonoBehaviour
{
    [Range(1,20)]
    public int minRows;
    [Range(1,20)]
    public int maxRows;
    [Range(1,20)]
    public int minCols;
    [Range(1,20)]
    public int maxCols;

    public string InputHeight { get; set; } //setter being used by textfield.
    public string InputWidth { get; set; } //setterbeing used by textfield;

    private int _rows;
    private int _cols;

    [SerializeField] GameObject WarningGO;
    [SerializeField] private GridGenerator grid;

    
    #region Monobehaviours
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnValidate()
    {
        if(minRows > maxCols || minRows > maxCols)
            Debug.LogError("invalid inputs");
    }

    #endregion
    
    #region PlayerInput

    public void GenerateButtonClicked()
    {
        
        if (InputValidation())
        {
            WarningGO.SetActive(false);
            grid.Generate(_rows, _cols);
        }
        else
        {
            WarningGO.SetActive(true);
        }
    }

    private bool InputValidation()
    {
        int rows;
        int cols;
        
        if (Int32.TryParse(InputWidth, out cols))
        {
            if (cols < minCols || cols > maxCols)
                return false;  //invalid range
        }
        else
        {
            return false; // couldnt parse.
        }
        
        if (Int32.TryParse(InputHeight, out rows))
        {
            if (rows < minCols || rows > maxCols)
                return false;  //invalid range
        }
        else
        {
            return false; // couldnt parse.
        }

        _rows = rows;
        _cols = cols;
        return true; //all tests passed.

    }

    #endregion
}
