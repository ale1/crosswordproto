﻿using System.Collections;
using UnityEngine;
using System;
using JetBrains.Annotations;
using UnityEngine.Networking;
using CW.Mock;

namespace CW
{
    [Serializable]
    public class CrossWebResponse
    {
        public int status;
        [CanBeNull] public string message;
        [CanBeNull] public string errors;
        public CrossData data;
    }

    [Serializable]
    public class CrossData
    { 
        public string[] rows;
        public DefinitionsData definitions;
        
        [Serializable]
        public struct DefinitionsData
        {
            public string[] across;
            public string[] down;
        }
    }




    public class APIController : MonoBehaviour
    {

        public bool useTestData = false;
    
        private string testJson = 
        @"{
            ""status"": 200,
            ""message"": null,
            ""errors"": null,
            ""data"": {
                ""rows"": [
                    ""n,a,t,i,v,e,-,r,a,i,l"",
                    ""i,r,o,n,e,d,-,i,r,m,a"",
                    ""c,o,p,s,e,s,-,s,t,a,t"",
                    ""e,m,i,t,s,-,b,e,i,g,e"",
                    ""r,a,c,y,-,p,e,n,s,i,n"",
                    ""-,-,-,l,e,e,r,-,a,n,t"",
                    ""-,t,w,e,n,t,y,o,n,e,-"",
                    ""b,e,e,-,t,e,l,l,-,-,-"",
                    ""l,a,n,c,e,r,-,d,e,p,p"",
                    ""a,s,t,o,r,-,s,t,a,l,l"",
                    ""s,h,a,w,-,m,a,i,t,a,i"",
                    ""t,o,p,e,-,i,m,m,u,n,e"",
                    ""s,p,e,d,-,s,e,e,p,e,d""
                ],
                ""definitions"": {
                    ""across"": [
                        ""Local resident"",
                        ""Subway feature"",
                        ""Pressed"",
                        ""'? La Douce'"",
                        ""Small groves"",
                        ""RBI or ERA"",
                        ""Issues"",
                        ""Pale brown"",
                        ""Risqu"",
                        ""Confines"",
                        ""Lewd look"",
                        ""Kitchen invader"",
                        ""Casino game"",
                        ""Flower visitor"",
                        ""Spill the beans"",
                        ""Cavalry soldier"",
                        ""'Edward Scissorhands' star"",
                        ""Famed fur tycoon"",
                        ""Barn section"",
                        ""'Pygmalion' playwright"",
                        ""Rum cocktail"",
                        ""Hit the bottle"",
                        ""Resistant, in a way"",
                        ""Went 70"",
                        ""Oozed""  
        ], 
    ""down"": [
    ""More polite"",
    ""Wild parties"",
    ""Bakery come-on"",
    ""Place to order scones"",
    ""Subject"",
    ""Became wild"",
    ""Fashionable"",
    ""Intimidated"",
    ""Some necklines"",
    ""Door sign"",
    ""Mag. workers"",
    ""Piper of rhyme"",
    ""Sue Grafton's '? for Malice'"",
    ""Transparent gem"",
    ""Matching"",
    ""Out of bed"",
    ""From bygone days"",
    ""Skilled worker"",
    ""Accept without question"",
    ""John Lennon song"",
    ""Shop tool"",
    ""Hidden"",
    ""Worked, as a trade""
        ]
        } } }";
    
   private string fetchDataURI = "https://ltk-crossword-prototype.com/api/?rows={0}&cols={1}";
   
   private int _rows;
   private int _cols;

   public Action<CrossData> OnDataReady;
   
   public void FetchData(int rows, int cols, Action<CrossData> callback)
   {
       _rows = rows;
       _cols = cols;
       StartCoroutine(GetDataFromAPI(OnAPISuccess,OnAPIFail));
       OnDataReady = callback;
   }
   
   IEnumerator GetDataFromAPI(Action<string> onSuccess, Action<string> onFailure)
   {
       string url = String.Format(fetchDataURI,_rows,_cols);
         
         UnityWebRequest www = UnityWebRequest.Get(url);
         www.timeout = 3;
         yield return www.SendWebRequest();

         if (www.isHttpError || www.isNetworkError)
         {
             onFailure(www.error);
         }
         else
         {
             Debug.Log("receiving real data");
             byte[] result = www.downloadHandler.data;
             string dataJson = System.Text.Encoding.Default.GetString(result);
             onSuccess(dataJson);
         }
   }

   private void OnAPISuccess(string json)
   {
       //on success, parse returned json
       CrossData data= ParseJson(json);
       OnDataReady?.Invoke(data);
   }

   private void OnAPIFail(string reason)
   {
       Debug.LogWarning("API get failed:" + reason);
       
       //On API failure, generate fake data instead.
       //two types of fake data: hardcoded json for debugging purposes, or use mockData generator for random data.

       CrossData data;
       
       if (useTestData)
       {
           data = ParseJson(testJson);
       }
       else //generate random mock data
       {
           MockDataGenerator mockDataGenerator = new MockDataGenerator(_rows,_cols);
           data = mockDataGenerator.GenerateCrossData();
       }

       OnDataReady?.Invoke(data);
   }

   private CrossData ParseJson(string json)
   {
       CrossWebResponse crossWebResponse = JsonUtility.FromJson<CrossWebResponse>(json);
       CrossData data = crossWebResponse.data;
       return data;
   }
    }
}
