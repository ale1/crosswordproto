﻿
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CW
{
    public class CrossWord : MonoBehaviour
    {
        #region singleton

        private static CrossWord _instance;

        public static CrossWord Instance
        {
            get { return _instance; }
        }

        private void Awake()
        {
            if (_instance != null && _instance != this)
                Destroy(this.gameObject);
            else
            {
                _instance = this;
            }
        }

        #endregion
        
        public Cell[,] Cells;

        public GridLayoutGroup Glg { get; private set; }
        public Text definitionBox;

        private Cell _selectedCell;

        public Dictionary<string, List<string>> WordsRevealed { get; private set; }
        
// Start is called before the first frame update
        void Start()
        {
            Glg = GetComponent<GridLayoutGroup>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnDestroy()
        {
         
        }

        public void Setup()
        {
            WordsRevealed = new Dictionary<string, List<string>>();
            WordsRevealed["across"] = new List<string>();
            WordsRevealed["down"] = new List<string>();
            
            foreach (var cell in Cells)
            {
                cell.OnBecomingSelected += ChangeSelectedCell;
                
                //calculate linked cells across
                int wordLengthAcross = cell.wordAcross.Length;
                if (wordLengthAcross > 0)
                {
                    List<Cell> linkedAcross = new List<Cell>(); 
                    int startCol = cell.Col - (int) cell.LetterPosAcross;
                    int endCol  = cell.Col  + (int) (wordLengthAcross - cell.LetterPosAcross);

                    for (int i = startCol; i < endCol; i++)
                    {
                        Cell targetCell = Cells[cell.Row, i];
                        if (targetCell != cell)
                        {
                            linkedAcross.Add(targetCell);
                        }
                    }
                    cell.linkedAcross = linkedAcross.ToArray();
                }
                
                //calculate linked cells down.
                int wordLengthDown = cell.wordDown.Length;
                if (wordLengthDown > 0)
                {
                    List<Cell> linkedDown = new List<Cell>();
                    int startRow = cell.Row - (int) cell.LetterPosDown;
                    int endRow  = cell.Row  + (int) (wordLengthDown - cell.LetterPosDown);
            
                    for (int i = startRow; i < endRow; i++)
                    {
                        Cell targetCell = Cells[i, cell.Col];
                        if (targetCell != cell)
                        {
                            linkedDown.Add(targetCell);
                        }
                    }
                    cell.linkedDown = linkedDown.ToArray();
                }
            }
        }
        
        public void AdjustToResolution(int rows, int cols)
        {
            Rect rect = Glg.GetComponent<RectTransform>().rect;
            float maxSize = Mathf.Min(rect.width / cols, rect.height / rows);
            Glg.cellSize = new Vector2(maxSize, maxSize); //adjust cell size dynamically according gto row/col count, ensuring its always squares.
        }

        public void ResolutionChangedOnRuntime()
        {
            Debug.Log("resolution changed during runtime");
            Rect rect = Glg.GetComponent<RectTransform>().rect;
            float maxSize = Mathf.Min(rect.width / Cells.GetLength(1), rect.height / Cells.GetLength(0));
            Glg.cellSize = new Vector2(maxSize, maxSize); 
        }

        private void ChangeSelectedCell(Cell newCell)
        {
            Cell oldCell = _selectedCell;
            if (oldCell && oldCell != newCell)
                _selectedCell.SetState(new CellStateIdle(_selectedCell));

            _selectedCell = newCell;
        }
    }
}
