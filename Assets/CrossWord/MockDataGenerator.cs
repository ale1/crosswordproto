﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using Random = UnityEngine.Random;

namespace CW.Mock
{
    public class MockDataGenerator
    {
        public const int MINBlack = 1;
        public const int MAXBlack = 5;

        public enum Side
        {
            Top,
            Bottom,
            Left,
            Right
        }
        
        
        public class Square   
        {
            //public Vector2Int[] pos;                 //todo optimization: cache pos
            public string Text = "?";
            public List<Neighbour> BlockingNeighbours = new List<Neighbour>();

            public List<Side> BlockedSides
            {
                get
                {
                    List<Side> blockedSides = BlockingNeighbours.Select(x => x.Side).ToList().Concat(BlockedEdges).Distinct().ToList();
                    return blockedSides;
                }
            }
            public List<Side> BlockedEdges = new List<Side>();

            public bool IsBlack => (Text == "-");

            public void Blacken(bool op)
            {
                Text = op ? "-" : SavedLetter;
            }

            public string SavedLetter;

        }
        
        public struct Neighbour
        {
            public Neighbour(Vector2Int pos, Side side)
            {
                this.Pos = pos;
                this.Side = side;
            }
            
            public readonly Side Side;
            public Vector2Int Pos;
        }
        
        private readonly Square[,] _squaresArray;
        private Vector2Int _size;
        
        public MockDataGenerator(int rows, int cols) //contructor
        {
            _size = new Vector2Int(rows, cols);
            _squaresArray = new Square[rows, cols];
            
            System.Random rnd = new System.Random();
  
            
            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    Square square = new Square();
                    _squaresArray[r, c] = square;
                    string letter = GetRandomLetter(rnd).ToString();
                    square.Text = letter;
                    square.SavedLetter = letter;
                    

                    if (r == 0)
                        square.BlockedEdges.Add(Side.Top);
                    
                    if(r == rows - 1)
                        square.BlockedEdges.Add(Side.Bottom);

                    if (c == 0) 
                        square.BlockedEdges.Add((Side.Left));
                    
                    if(c == cols - 1)
                        square.BlockedEdges.Add((Side.Right));

                }
            }

            //pick a random amount of squares for this grid, with min being 1 per row/col, and max 5 per row/col.
            int largestAxis = Math.Max(rows, cols);
            int blackQuota = Random.Range(largestAxis * MINBlack, largestAxis * MAXBlack + 1);

            //pass through each row and add 1 black in random location, to make ensure base grid is legal.
            for (int r = 0; r < rows; r++) //debug
            {
                int[] colIndexes = Enumerable.Range(0, cols).ToArray();
                RandomizeIntArray(colIndexes); //shuffled in place colIndexes.

                foreach(var randomCol in colIndexes)
                {
                    Square square = _squaresArray[r, randomCol];
                    
                    square.Blacken(true);
                    RefreshBlockers();
                    if (Validate(r, randomCol))
                    {
                        blackQuota--;
                        break;
                    }
                    else //undo
                    {
                        square.Blacken(false);
                        RefreshBlockers();
                    }
                }
            }
            
            
            //pass through each column, and add a black square if col doesnt have one already, to ensure base grid is legal.
            for (int c = 0; c < cols; c++)
            {
                bool alreadyHasBlack = false;
                for (int r = 0; r < _size.x; r++)
                {
                    Square square = _squaresArray[r, c];
                    if (square.IsBlack)
                    {
                        alreadyHasBlack = true;
                        break;
                    }
                }

                if (alreadyHasBlack) //col already randomly got a black square when we populated rows above; lets skip this col.
                    continue;

                
                int[] rowIndexes = Enumerable.Range(0, rows).ToArray();
                RandomizeIntArray(rowIndexes); //shuffled in place rowIndexes.

                foreach (var randomRow in rowIndexes)
                {
                    Square square = _squaresArray[randomRow, c];
                    
                    square.Blacken(true);   //todo refactor into method.
                    RefreshBlockers();
                    if (Validate(randomRow, c))
                    {
                        blackQuota--;
                        break;
                    }
                    else
                    {
                        square.Blacken(false);
                        RefreshBlockers();
                    }
                }
            }

            if (blackQuota <= 0)
                    return;

            //grid is already legal at this point, but we want to further add black squares to match target random quota of  black.
            #region BlackQuota
            //todo refactor:unsure how I feel about using local function to break out of nested for loop... might consider refactoring it out.
            FillBlackQuota();
            void FillBlackQuota()
            {
                int[] rowIndexes = Enumerable.Range(0, _size.x).ToArray();
                int[] colIndexes = Enumerable.Range(0, _size.y).ToArray();
                
                //we randomly shuffle the order in which we iterate over squares.
                RandomizeIntArray(rowIndexes);
                RandomizeIntArray(colIndexes);
                    
                foreach (int r in rowIndexes)
                {
                    foreach (int c in colIndexes)
                    {
                        Square square = _squaresArray[r, c];

                        if (blackQuota <= 0) // we've met quota. end prematurely.
                            return; //exits local function only

                        if(square.IsBlack)  
                            continue;

                        square.Blacken(true);
                        RefreshBlockers();
                        if (Validate(r, c ))
                        {
                            blackQuota--;
                            break;
                        }
                        else  //undo
                        {
                            square.Blacken(false);
                            RefreshBlockers();
                        }
                    }
                }
            }
                #endregion
        }
        
        //small helper method
        public static void RandomizeIntArray(int[] arr)
        {
            for (var i = arr.Length - 1; i > 0; i--) {
                var r = Random.Range(0,i);
                var tmp = arr[i];
                arr[i] = arr[r];
                arr[r] = tmp;
            }
        }

        private Neighbour[] FetchNeighbours(int row, int col)
        {
            Neighbour top =  new Neighbour( new Vector2Int(row - 1, col), Side.Top);   
            Neighbour bottom = new Neighbour(new Vector2Int(row + 1, col), Side.Bottom);
            Neighbour right = new Neighbour(new Vector2Int(row, col + 1), Side.Right);
            Neighbour left = new Neighbour(new Vector2Int(row, col - 1), Side.Left);
            Neighbour[] neighbours =  {top, bottom, right, left};
            return neighbours;
        }
        
        

        public void RefreshBlockers()  //todo optimization: only refresh nearby cells, not all
        {
            for (int r = 0; r < _size.x; r++)  
            {
                for (int c = 0; c < _size.y; c++)
                {
                    Neighbour[] neighbours = FetchNeighbours(r,c);   //todo: optimize by caching neighbours instead of recalculating every time.
                    
                    Square square = _squaresArray[r, c];
                    square.BlockingNeighbours = new List<Neighbour>();
                    foreach (var neighbour in neighbours)
                    {
                        Vector2Int pos = neighbour.Pos;
                        
                        if(pos.x < 0 || pos.x >= _size.x || pos.y < 0 || pos.y >= _size.y)  //invalid neighbour, since outside edge
                            continue;

                        Square neighbourSquare = _squaresArray[pos.x, pos.y];
                        if(neighbourSquare.IsBlack)
                            square.BlockingNeighbours.Add(neighbour);
                        else
                        {
                            square.BlockingNeighbours.Remove(neighbour);
                        }
                    }
                }
            }
        }

        public bool Validate(int row, int col)
        {
            //neighbours   //todo: optimize by caching neighbours instead of recalculating every time.
            Neighbour[] neighbours = FetchNeighbours(row, col);
            
            foreach (var neigh in neighbours)
            {
                Vector2Int pos = neigh.Pos;
                if (pos.x < 0 || pos.x >= _size.x || pos.y < 0 || pos.y >= _size.y
                ) //invalid neighbour, since outside edge, dont test.
                    continue;

                Square square = _squaresArray[pos.x, pos.y];

                if (square.IsBlack)
                    continue; //if square is black its unaffected, pass
                
                
                if (square.BlockedSides.Count() >= 3)
                    return false;

                
                if (square.BlockedSides.Count() == 2) //check that the two blocked sides arent opposites.
                {
                    if (square.BlockedSides.Contains(Side.Top) && square.BlockedSides.Contains(Side.Bottom))
                       return false;

                    if (square.BlockedSides.Contains(Side.Left) && square.BlockedSides.Contains(Side.Right))
                        return false;
                }
            }

            
            //check that row/col hasnt reached max amount of black squares
            List<Square> rowSiblings = new List<Square>();   //todo: optimization:  cache this info in square struct instead of recalculating each time.
            List<Square> colSiblings = new List<Square>();  
            
            for(int i = 0; i < _size.y;i++)
            {
                rowSiblings.Add(_squaresArray[row,i]);
            }
            int rowBlackCount = rowSiblings.Count(s => s.IsBlack);
            if (rowBlackCount > MAXBlack)   //too many black squares in row.
                return false;
            
            for(int i = 0; i < _size.x;i++)
            {
                colSiblings.Add(_squaresArray[i,col]);
            }
            int colBlackCount = colSiblings.Count(s => s.IsBlack);
            if (colBlackCount > MAXBlack)   //too many black squares in col.
                return false;
            
            return true; // hooray! we passed all tests
        }

        public CrossData GenerateCrossData()
        {
            //extract strings from SquareArray into desired row format.
            List<string> rowsList = new List<string>();
            for (int x = 0; x < _size.x; x++)
            {
                List<string> rowString = new List<string>();
                for(int y = 0; y < _size.y; y++)
                {
                    rowString.Add(_squaresArray[x,y].Text);
                }
                rowsList.Add(String.Join(",",rowString));
            }

            CrossData mockData = new CrossData();
            mockData.rows = rowsList.ToArray();
            
            #region definitions

            List<string> acrossDefinitions = new List<string>();
            foreach (var rowString in rowsList)
            {
                List<string> rowWords = rowString.Split('-').ToList();
                for (int i = 0; i < rowWords.Count(); i++)
                    rowWords[i] = rowWords[i].Replace(",", "");
                
                rowWords = rowWords.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();
                List<string> rowDefinitions = rowWords.Select(w => "definition of " + w).ToList();
                acrossDefinitions.AddRange(rowDefinitions);
            }
            
            
            //extract strings from SquareArray into desired col format.
            List<string> colsList = new List<string>();
            for (int c = 0; c < _size.y; c++)
            {
                List<string> colString = new List<string>();
                for(int r = 0; r < _size.x; r++)
                {
                    colString.Add(_squaresArray[r,c].Text);
                }
                colsList.Add(String.Join(",",colString));
            }
            
            List<string> downDefinitions = new List<string>();
            foreach (var colsString in colsList)
            {
                List<string> colWords = colsString.Split('-').ToList();
                for (int i = 0; i < colWords.Count(); i++)
                    colWords[i] = colWords[i].Replace(",", "");
                
                colWords = colWords.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();
                List<string> colDefinitions = colWords.Select(w => "definition of " + w).ToList();
                downDefinitions.AddRange(colDefinitions);
            }

            CrossData.DefinitionsData definitions = new CrossData.DefinitionsData
            {
                across = acrossDefinitions.ToArray(), 
                down = downDefinitions.ToArray()
            };

            mockData.definitions = definitions;
            #endregion

            
            return mockData;
        }

        
        private readonly float probVowel = 0.5f - 5 / 26f;
        private readonly char[] _vowels = {'a', 'e', 'i', 'o', 'u'};
        
        private char GetRandomLetter(System.Random rnd)
        {
            if (Random.value < probVowel)
            {
                int index = rnd.Next(_vowels.Length);
                return _vowels[index];
            }
            else
            {
                int num = rnd.Next(0, 26);
                char letter = (char)('a' + num);
                return letter;
            }
        }
        
    }
    


}