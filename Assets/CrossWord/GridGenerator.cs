﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace CW
{
    public class GridGenerator : MonoBehaviour
    {
        [SerializeField] private APIController API;
        [SerializeField] private CrossWord crossWord;
        [SerializeField] private GameObject cellPrefab;
        
        private bool _busy = false;  //prevents click-abuse.
        
        #region MonoBehaviour
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion
        
        public void Generate(int rows, int cols)
        {
            if (_busy) return;
            
            _busy = true;
            API.FetchData(rows, cols,PopulateGrid);
            
        }
        
        private void PopulateGrid(CrossData data)
        {
            int rows = data.rows.Length; //number of rows of grid being generated
            int cols = data.rows[0].Split(',').Length; //number of cols of grid being generated
            
            #region setupGridLayout
            GridLayoutGroup glg = crossWord.Glg;
            ClearGrid(); //clear previous grid.
            crossWord.AdjustToResolution(rows, cols);
            glg.constraint = GridLayoutGroup.Constraint.FixedRowCount;
            glg.constraintCount = rows;
            #endregion
            
            crossWord.Cells = new Cell[rows, cols];
            Debug.Log("creating new grid of size:" + rows + " X " + cols);

            List<string>
                acrossDefinitions =
                    data.definitions.across.Reverse()
                        .ToList(); //reverse since we are traversing row from bottom to top.
            List<string> downDefinitions = data.definitions.down.ToList();


            //down vars
            string[] downData = new string[cols];

            #region AcrossDataParsing

            for (int r = rows - 1; r >= 0; r--)
            {
                string[] rowLetters = data.rows[r].Split(','); //e.g "n,a,t,i,v,e,-,r,a,i,l" => "[n,a,t,i,v,e,-,r,a,i,l]
                string rowString = data.rows[r].Replace(",", ""); //e.g "n,a,t,i,v,e,-,r,a,i,l" => "native-rail";
                string[] rowWords = rowString.Split('-'); //e.g "native-rail" => ["native","","rail"];
                rowWords = rowWords.Where(x => !string.IsNullOrEmpty(x)).ToArray(); //remove empties
                string wordAcross = "";
                string definitionAcross = "";

                List<string> rowDefinitions = acrossDefinitions.Take(rowWords.Length).Reverse().ToList();
                acrossDefinitions.RemoveRange(0, rowWords.Length);

                int letterCounter = 0; //counts letters per word
                int wordCounterRow = 0; //counts words per row

                for (int c = 0; c < cols; c++)
                {
                    int? letterPos = null;
                    string letter = rowLetters[c];
                    if (letter != "-") //is part of word    //todo: refactor to use char.
                    {
                        wordAcross = rowWords[wordCounterRow];
                        definitionAcross = rowDefinitions[wordCounterRow];
                        letterPos = letterCounter;
                        letterCounter++;
                    }
                    else //has reached separator
                    {
                        letterCounter = 0;
                        if (!String.IsNullOrEmpty(wordAcross))
                        {
                            wordAcross = "";
                            definitionAcross = "";
                            wordCounterRow++;
                        }
                    }

                    GameObject go = Instantiate(cellPrefab, crossWord.Glg.transform);
                    Cell cell = go.GetComponent<Cell>();

                    cell.Setup(r, c, letter, letterPos, wordAcross, definitionAcross);
                    crossWord.Cells[r, c] = cell;

                    //Generate DownData
                    downData[c] = rowLetters[c] + downData[c]; //add to front of string
                }
            }

            #endregion

            #region DownDataParsing
            //Replicate logic above but with DownData
            for (int c = 0; c < cols; c++)
            {
                char[] colLetters = downData[c].ToCharArray();
                string colString = downData[c].Replace(",", ""); //e.g "n,a,t,i,v,e,-,r,a,i,l" => "native-rail";
                string[] colWords = colString.Split('-'); //e.g "native-rail" => ["native","","rail"];
                colWords = colWords.Where(x => !string.IsNullOrEmpty(x)).ToArray(); //remove empties
                string wordDown = "";
                string definitionDown = "";

                List<string> colDefinitions = downDefinitions.Take(colWords.Length).ToList();
                downDefinitions.RemoveRange(0, colWords.Length);

                int letterCounter = 0; //counts letters per word
                int wordCounterCol = 0; //counts words per row

                for (int r = 0; r < rows; r++)
                {
                    int? letterPos = null;
                    char letter = colLetters[r];

                    if (letter != Char.Parse("-")) //is part of word
                    {
                        wordDown = colWords[wordCounterCol];
                        definitionDown = colDefinitions[wordCounterCol];
                        letterPos = letterCounter;
                        letterCounter++;
                    }
                    else //has reached separator
                    {
                        letterCounter = 0;
                        if (!String.IsNullOrEmpty(wordDown))
                        {
                            wordDown = "";
                            definitionDown = "";
                            wordCounterCol++;
                        }
                    }

                    Cell targetCell = crossWord.Cells[r, c];
                    targetCell.wordDown = wordDown;
                    targetCell.definitionDown = definitionDown;
                    targetCell.LetterPosDown = letterPos;
                }

                //Finish setup
                crossWord.Setup();
            }

            #endregion

            _busy = false;
        }
        
        private void ClearGrid()
        {
            foreach (Transform child in crossWord.transform)
            {
                Destroy(child.gameObject);
            }
            
        }
        



    }
}
