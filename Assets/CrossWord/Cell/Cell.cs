﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

namespace CW
{
    public class Cell : MonoBehaviour, IPointerClickHandler
    {
        public Image Image;
        
        public Text letterText;
        public string letter;

        public int? LetterPosAcross; //relative pos of letter within word. e.g "a in rail" => 1
        public string wordAcross;
        public string definitionAcross;
     
        public int? LetterPosDown;
        public string wordDown;
        public string definitionDown;

        public int Row { get;private set; }
        public int Col { get; private set; }
        
        public Cell[] linkedAcross;  //sister cells that belong to same word across.
        public Cell[] linkedDown; // sister cells that belong to same word down.
        
        public Action<Cell> OnBecomingSelected;

        public int[] Cord => new int[]{ Row,Col }; 
        
        #region States
        private CellState _currentState;
        public void SetState(CellState newState)
        {
            if (_currentState != null && _currentState.GetType() == newState.GetType())
            {
                Debug.LogError(String.Format("trying to enter {0} state when {1} already in it", _currentState.GetType(), String.Join(",",Cord)));
                return;
            }

            if (_currentState != null)
                _currentState.OnStateExit();
            
            _currentState = newState;
            gameObject.name = String.Concat("Cell in: ", _currentState.GetType().Name);
            _currentState.OnStateEnter();
        }
        
        public bool IsState<T>()
        {
            return _currentState.GetType() == typeof(T) ;
        }
        #endregion
        
        
        // Start is called before the first frame update
        void Start()
        {
            Image = GetComponent<Image>();
        }

        // Update is called once per frame
        void Update()
        {
            if (_currentState != null)
                _currentState.OnUpdate();
        }

        
        public void Setup(int row, int col, string letter, int? letterPosAcross, string acrossWord, string definition)
        {
            this.Row = row;
            this.Col = col;
            this.letter = letter;
            this.LetterPosAcross = letterPosAcross;
            this.wordAcross = acrossWord;
            this.definitionAcross = definition;
            
            //set inital state
            switch (letter.Trim())
            {
                case "-":
                    SetState(new CellStateBlack(this));
                    break;
                default:
                    SetState(new CellStateHidden(this));
                    break;
            }
        }
        
        #region PointerHandlers

        public void OnPointerClick(PointerEventData eventData)
        {
            _currentState.onPointerClick(eventData);
        }


        #endregion


    }
}
