﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CW
{


    public class CellStateBlack : CellState
    {
        public CellStateBlack(Cell cell) : base(cell)  //constructor
        {
            
        }
        
        public override void OnStateEnter()
        {
            Cell.Image.color = Color.black;
            Cell.letterText.text = "";
            Transitioning = false;
        }

        public override void OnStateExit()
        {
            Transitioning = true;
        }
        
        public override void OnUpdate()
        {
            if (Transitioning) return;
            
            
        }

        #region PointerHandlers

        public override void onPointerClick(PointerEventData clickData)
        {
            //do nothing;
        }
        
        #endregion
    }

}