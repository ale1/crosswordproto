﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CW
{
    public class CellStateSelected : CellState
    {
        //state specific vars
        public enum Focus
        {
            None,
            Across,
            Down
        }

        private Focus _currentFocus = Focus.None;

        public CellStateSelected(Cell cell) : base(cell)  //constructor
        {
           //this state is unique, when a new one is being created, tell crossowrd to switch out old one.
           Cell.OnBecomingSelected?.Invoke(Cell);
        }
        
        public override void OnStateEnter()
        {

            var dictionary = CrossWord.Instance.WordsRevealed;

            if (!dictionary["across"].Contains(Cell.wordAcross))
            {
                dictionary["across"].Add(Cell.wordAcross);
                _currentFocus = Focus.Across;
            }
            else
            {
                _currentFocus = Focus.Down;
                
                if(!dictionary["down"].Contains(Cell.wordDown))
                    dictionary["down"].Add(Cell.wordDown);
            }
            Cell.Image.color = new Color(1.0f, 0.64f, 0.0f);
            Cell.letterText.text = Cell.letter;
           
            HightlightWord();

            Transitioning = false;
        }

        public override void OnStateExit()
        {
            Transitioning = true;
            _currentFocus = Focus.None;
            HightlightWord();
        }
      
        
        public override void OnUpdate()
        {
            if (Transitioning) return;
            
            //do nothing.
        }

        #region PointerHandlers

        public override void onPointerClick(PointerEventData clickData)
        {
            Debug.Log(String.Format("Toggling cell on {0}", String.Join(",",Cell.Cord)));
            ToggleFocus();
        }
        #endregion


        private void HightlightWord()
        {
            if(_currentFocus == Focus.None)
            {
                foreach (var linkedCell in Cell.linkedAcross)
                {
                    if(linkedCell.IsState<CellStateHighlight>())
                        linkedCell.SetState(new CellStateIdle(linkedCell));
                }
                foreach (var linkedCell in Cell.linkedDown)
                {
                    if(linkedCell.IsState<CellStateHighlight>())
                        linkedCell.SetState(new CellStateIdle(linkedCell));
                }

                CrossWord.Instance.definitionBox.text ="";
            }
            else if (_currentFocus == Focus.Across)
            {
                foreach (var linkedCell in Cell.linkedDown)
                {
                    if(linkedCell.IsState<CellStateHighlight>())
                        linkedCell.SetState(new CellStateIdle(linkedCell));
                }
                foreach (var linkedCell in Cell.linkedAcross)
                {
                    linkedCell.SetState(new CellStateHighlight(linkedCell));
                }
                
                CrossWord.Instance.definitionBox.text = Cell.definitionAcross;
            }
            else
            {
                foreach (var linkedCell in Cell.linkedAcross)
                {
                    if(linkedCell.IsState<CellStateHighlight>())
                        linkedCell.SetState(new CellStateIdle(linkedCell));
                }
                foreach (var linkedCell in Cell.linkedDown)
                {
                    linkedCell.SetState(new CellStateHighlight(linkedCell));
                }
                
                CrossWord.Instance.definitionBox.text = Cell.definitionDown;
            }
        }
        

        private void ToggleFocus()
        {
            if (_currentFocus == Focus.Across)
            {
                if(Cell.wordDown.Length > 0)   //dont allow toggling focus if no word in that direction.
                    _currentFocus = Focus.Down;
            }
            else
            {
                if(Cell.wordAcross.Length > 0)  //dont allow toggling focus if no word in that direction.
                    _currentFocus = Focus.Across;
            }
            HightlightWord();
        }
        
        
        
        
    }
}