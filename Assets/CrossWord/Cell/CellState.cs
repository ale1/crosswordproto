﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CW
{


    public abstract class CellState
    {
        protected Cell Cell;
        protected bool Transitioning = false;

        public CellState(Cell cell)
        {
            this.Cell = cell;
        }
        
        //Base StateMachine behaviours
        public abstract void OnUpdate();
        public abstract void OnStateEnter();
        public abstract void OnStateExit();
        
        //Pointer Events
        public abstract void onPointerClick(PointerEventData clickData);

    }

}
