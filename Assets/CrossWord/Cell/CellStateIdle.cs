﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CW
{
    public class CellStateIdle : CellState
    {
        


        public CellStateIdle(Cell cell) : base(cell)  //constructor
        {
            
        }
        
        public override void OnStateEnter()
        {
            Cell.Image.color = Color.white;
            Cell.letterText.text = Cell.letter;
            Transitioning = false;
        }

        public override void OnStateExit()
        {
            Transitioning = true;
        }
        
        public override void OnUpdate()
        {
            if (Transitioning) return;
            
            
        }

        #region PointerHandlers

        public override void onPointerClick(PointerEventData clickData)
        {
            Debug.Log(String.Format("CellStateHidden: Selecting cell on row {0}, col {1}", Cell.Row, Cell.Col));
            Cell.SetState(new CellStateSelected(Cell));

        }
        #endregion


  
     
        
    }
}